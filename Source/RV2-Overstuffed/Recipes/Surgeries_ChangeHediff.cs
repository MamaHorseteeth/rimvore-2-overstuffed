using RimWorld;
using RimVore2;
using System;
using System.Collections.Generic;
using Verse;

namespace RV2_Overstuffed
{

    public class RecipeDef_SurgeryChangeHediff : RecipeDef
    {
        public float rollStrength;

        public FloatRange desiredRange;

        public void ExposeData()
        {
            Scribe_Values.Look(ref rollStrength, "rollStrength");
            Scribe_Values.Look(ref desiredRange, "desiredRange");
        }
    }

    public class Recipe_SurgeryChangeHediff : Recipe_Surgery
    {

        public override IEnumerable<BodyPartRecord> GetPartsToApplyOn(Pawn pawn, RecipeDef recipe)
        {
            var recipeDef = ((RecipeDef_SurgeryChangeHediff)recipe);
            List<Hediff> allHediffs = pawn.health.hediffSet.hediffs;
			for (int i = 0; i < allHediffs.Count; i++)
			{
				if (allHediffs[i].Part != null)
				{
					if (allHediffs[i].def == recipe.addsHediff)
					{
                        // Only applicable to hediffs with severity outside of the desired range
						if (allHediffs[i].Visible && !recipeDef.desiredRange.Includes(allHediffs[i].Severity))
						{
							yield return allHediffs[i].Part;
						}
					}
				}
			}
        }

        public override void ApplyOnPawn(Pawn pawn, BodyPartRecord part, Pawn billDoer, List<Thing> ingredients, Bill bill)
        {
            var recipeDef = ((RecipeDef_SurgeryChangeHediff)bill.recipe);
            if (billDoer != null)
			{
				if (base.CheckSurgeryFail(billDoer, pawn, ingredients, part, bill))
				{
					return;
				}
				TaleRecorder.RecordTale(TaleDefOf.DidSurgery, new object[]
				{
					billDoer,
					pawn
				});
			}
			Hediff hediff = pawn.health.hediffSet.hediffs.Find((Hediff x) => x.def == this.recipe.addsHediff && x.Part == part && x.Visible);
			if (hediff != null)
			{
                var oldSeverity = hediff.Severity;
                if (hediff.Severity < recipeDef.desiredRange.min) {
                    // Less than the min, increase and then clamp to make sure we don't overshoot
                    hediff.Severity = Math.Min(hediff.Severity + recipeDef.rollStrength, recipeDef.desiredRange.max);
                } else if (hediff.Severity > recipeDef.desiredRange.max) {
                    // Greater than the max, decrease and then clamp to make sure we don't overshoot
                    hediff.Severity = Math.Max(hediff.Severity - recipeDef.rollStrength, recipeDef.desiredRange.min);
                }
                RV2Log.Message("Recipe_SurgeryChangeHediff for " + pawn.LabelShort + ": changing " + hediff.Label + " from " + oldSeverity + " to " + hediff.Severity, "Surgery");
			}
        }
    }
}
